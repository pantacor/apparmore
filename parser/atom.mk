
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := apparmor-parser
LOCAL_DESCRIPTION := apparmor parser part
LOCAL_LIBRARIES := apparmor

LOCAL_AUTOTOOLS_MAKE_BUILD_ARGS := CROSS_COMPILE=$(TARGET_CROSS) \
	CC=$(TARGET_CROSS)gcc \
	CXX=$(TARGET_CROSS)g++ \
	BUILD_CC=gcc \
	INCLUDEDIR=$(TARGET_OUT_STAGING)/usr/include \
	LIBAPPARMOR_A=$(TARGET_OUT_STAGING)/usr/lib/libapparmor.a \
	AARE_LDFLAGS="-L $(TARGET_OUT_STAGING)/usr/lib/" \
	USE_SYSTEM=1
LOCAL_AUTOTOOLS_MAKE_INSTALL_ARGS := lib=lib \
	INCLUDEDIR=$(TARGET_OUT_STAGING)/usr/include \
	LIBAPPARMOR_A=$(TARGET_OUT_STAGING)/usr/lib/libapparmor.a \
	AARE_LDFLAGS="-L $(TARGET_OUT_STAGING)/usr/lib/" \
	USE_SYSTEM=1

define LOCAL_AUTOTOOLS_CMD_CONFIGURE
	cp -r $(PRIVATE_SRC_DIR)/*  $(call module-get-build-dir,apparmor-parser)/obj
	cp -r $(PRIVATE_SRC_DIR)/../common  $(call module-get-build-dir,apparmor-parser)/common
	rm -f $(call module-get-build-dir,apparmor-parser)/obj/atom.mk
endef

include $(BUILD_AUTOTOOLS)

